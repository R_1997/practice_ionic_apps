import {
  IonApp,
  IonContent,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonLabel,
  IonItem,
  IonDatetime,
} from '@ionic/react';
import React, { useState } from 'react';
import BioRhythmCard from './components/biocard.js'
import { useLocalStorage } from './storehook'

function App() {
  const [birthDate, setbirthDate] = useLocalStorage('birthDate','');
  const [targetDate, settargetDate] = useState(new Date().toISOString());
  return (
    <IonApp>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>Biorythms</IonTitle>
        </IonToolbar>
      </IonHeader >
      <IonContent className="ion-padding" >
        
        <IonItem>
          <IonLabel position = "floating">Birth Date:</IonLabel>
          <IonDatetime displayFormat = "D-MMM-YYYY"
            value ={birthDate} 
            onIonChange = {(event) => setbirthDate(event.detail.value)}/>
        </IonItem>
        <IonItem>
          <IonLabel position = "floating">Target Date:</IonLabel>
          <IonDatetime displayFormat = "D-MMM-YYYY"
            value ={targetDate} 
            onIonChange = {(event) => settargetDate(event.detail.value)}/>
        </IonItem>
        {birthDate &&
          <BioRhythmCard birthDate = {birthDate} targetDate = {targetDate}/>
        }
      </IonContent>
    </IonApp>
  );
}

export default App;
