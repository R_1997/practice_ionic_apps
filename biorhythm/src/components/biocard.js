import { 
    IonCard, 
    IonCardContent, 
    IonCardHeader,
    IonRow, 
    IonTitle,
    IonToggle,
} from '@ionic/react';
import React, { useState } from 'react';
import dayjs from 'dayjs';
import { calculateBiorhythms } from '../calculations';
import BiorhythmChart from './biochart';
import "./biocard.css"

function formatDate(isoString){
    return dayjs(isoString).format('D MMM YYYY');
}


function BioRhythmCard({ birthDate, targetDate } ){
    const [checked, setChecked]= useState('true')
    const {physical, emotional, intellectual} = 
        calculateBiorhythms(birthDate, targetDate);
    
    return(
        <IonCard className="biorhythm-card ion-text-center">
        <IonCardHeader>
          <IonTitle>{ formatDate(targetDate) }</IonTitle>
        </IonCardHeader>
        <IonCardContent>
            <p className="physical">Physical:{physical.toFixed(4)}</p>
            <p className="emotional">Emotional:{emotional.toFixed(4)} </p>
            <p className="intellectual">Intellectual:{intellectual.toFixed(4)} </p>
        </IonCardContent>
        <IonRow >
                <IonToggle
                mode='ios'
                checked={checked} 
                onIonChange={event => setChecked(event.detail.checked)}
                className = "ion-padding"
                />
                    {checked &&
                        <BiorhythmChart birthDate={birthDate} targetDate={targetDate} />
                    }
        </IonRow>
      </IonCard>
    
    );
}

export default BioRhythmCard;